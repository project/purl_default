This module extends PURL, allowing you to nominate one modifier that will not appear in the URL.  Our original use case for this module was to enable us to use PURL with spaces_taxonomy to set a default space, so our default space would be activated when there was no modifier present in the URL.  This allows us to achieve a multi-site-like architecture, where the default space is activated with no modification to the path.

We currently expose one additional processor to PURL - "Path Default".  When used with Spaces module, this will allow each of your spaces to be activated on a path prefix as usual, but you can also set one space to be activated with no modifier being present in the path. 


Steps to use:


* Install the Purl Default module
* Enable the "Path Default" type, go to: /admin/settings/purl/types
* Assign the "Path Default" type to your desired provider, go to: /admin/settings/purl
* In order to set the default space, use a single dash as the modifier "-"
